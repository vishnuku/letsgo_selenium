from selenium import webdriver
import urllib.parse
import requests
import os
import random
fetch_url = 'https://letsgo.travelsmarter.app/2020/04/12/chase-freedom-unlimited-why-you-should-have-this-card/'

class ClickAdvertise():

    def __init__(self, ads_id_list, href_list):
        print('hello')
        options = webdriver.ChromeOptions()

        username = 'lum-customer-mgdesign-zone-static'
        password = '4bnpu8s7mixc'
        port = 22225
        session_id = random.random()
        super_proxy_url = ('http://%s-country-us-session-%s:%s@zproxy.lum-superproxy.io:%d' %(username, session_id, password, port))
        
        # # seems some issue with proxy server otherwise it is working perfectly
        # options.add_argument('--proxy-server=%s' % super_proxy_url)


        self.browser = webdriver.Chrome('./chromedriver', options=chrome_options)
        self.browser.get(fetch_url)
        self.ads_id_list = []
        self.href_list = []
        self.url_list = []
        try:
            ids = self.browser.find_elements_by_xpath('//*[@id]')
            for i in ids:
                if i.get_attribute('id').startswith('google_ads') and 'container' not in i.get_attribute('id'):
                    print(i.get_attribute('id'))
                    self.ads_id_list.append(i.get_attribute('id'))
                else:
                    pass
        except:
            print('no google ads in this HTMl session')

    # calling get_href_link method
    def get_link(self):
        for ad_id in self.ads_id_list:
            self.get_href_link(ad_id)
        return self.href_list

    def get_href_link(self, ad_id):
        # self.browser.switch_to.parent_frame()
        self.browser.switch_to.frame(ad_id)
        anchor_tag = self.browser.find_element_by_xpath("//a")
        href = anchor_tag.find_elements_by_xpath('//*[@href]')
        for h in href:
            if urllib.parse.quote_plus(h.get_attribute('href')).startswith('https%3A%2F%2Fgoogleads.g.doubleclick.net') and h.get_attribute('href') not in self.href_list :
                self.href_list.append(h.get_attribute('href'))
                print(h.get_attribute('href'))
        self.browser.switch_to.parent_frame()

    def create_csv(self):
        self.href_list = set(self.href_list)
        if self.href_list:
            import csv
            file_name = 'google__ads_href.csv'
            file_exists = os.path.isfile(file_name)
            with open(file_name, mode='a') as csv_file:
                fieldnames = ['Name', 'Link', 'original url']
                writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
                if not file_exists:
                    writer.writeheader()
                for href in self.href_list:
                    original_link = requests.get(href)
                    url = original_link.url
                    writer.writerow({'Name': 'Google Ads', 'Link': href, 'original url': url})

if __name__ == '__main__':
    click = ClickAdvertise(ads_id_list=[''], href_list=[''])
    click.get_link()
    click.create_csv()

